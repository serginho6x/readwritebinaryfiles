import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FicherosBinarios {
	public static void files () {
		String fileName = "out.bin";
		try {
			FileOutputStream fileOs = new FileOutputStream(fileName);
			ObjectOutputStream os = new ObjectOutputStream(fileOs);
			os.writeInt(2048);
			os.writeDouble(3.1415);
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done writing. Now reading...");
		
		try {
			FileInputStream fileIs = new FileInputStream(fileName);
			ObjectInputStream is = new ObjectInputStream(fileIs);
			int x = is.readInt();  //do it in the same order
			System.out.println(x);
			double d = is.readDouble(); 
			System.out.println(d);
			is.close();
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void escribir() {
		File file;
		FileOutputStream fileOutputStream;
		ObjectOutputStream objectOutputStream;
		
		file = new File("FicherosBinarios.bin");
		try {
			fileOutputStream = new FileOutputStream(file);
			objectOutputStream = new ObjectOutputStream(fileOutputStream);
			
			Persona persona = new Persona();
			persona.setNombre("Romeo");
			persona.setEdad(20);
			persona.setDni(76542342);
			
			Persona persona1 = new Persona();
			persona1.setNombre("Maria");
			persona1.setEdad(19);
			persona1.setDni(44444444);
			
			Persona persona2 = new Persona();
			persona2.setNombre("Mario");
			persona2.setEdad(50);
			persona2.setDni(5555555);
			
			objectOutputStream.writeObject(persona);
			objectOutputStream.writeObject(persona1);
			objectOutputStream.writeObject(persona2);
			
		} catch (FileNotFoundException ex) {
			
		} catch (IOException ex) {
			
		}
	}
	
	void leer() {
		File file;
		FileInputStream fileInputStream;
		ObjectInputStream objectInputStream;
		
		file = new File("FicherosBinarios.bin");
		try {
			fileInputStream = new FileInputStream(file);
			objectInputStream = new ObjectInputStream(fileInputStream);
			
			
			while(true) {
				Object data = objectInputStream.readObject();
				Persona persona = (Persona)data;
				System.out.println("\n");
				System.out.println("Nombre: " + persona.getNombre());
				System.out.println("Edad: " + persona.getEdad());
				System.out.println("DNI: " + persona.getDni());
			}
			
			
		} catch (FileNotFoundException ex) {
			
		} catch (IOException | ClassNotFoundException ex) {
			
		}
		
	}
}
